package cz.cvut.fel.pjv.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Board {    // Caretaker

    private PlayerGold gold;
    private PlayerSilver silver;
    private Originator originator;        // is current state of game (current snapshot)
    private Stack<Memento> history;


    public void makeMove() {
        /**
         * Game state was changed, so save the change
         */
        Memento m = originator.save();
        history.push(m);
    }

    public void undo() {
        Memento m = history.pop();
        originator.restore(m);
    }

    public void saveToFile() {
        // save this snapshot of game when app is turned off, write to file last snapshot
    }
}
