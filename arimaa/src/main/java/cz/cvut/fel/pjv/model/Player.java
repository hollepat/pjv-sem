package cz.cvut.fel.pjv.model;

import cz.cvut.fel.pjv.pieces.Piece;

public interface Player {

    public void setUpPieces();
    public void movePiece(Piece piece, char y, int x );


}
