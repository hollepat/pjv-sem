package cz.cvut.fel.pjv.model;

import cz.cvut.fel.pjv.pieces.Piece;

public class Spot {


    private TypeOfSpot typeOfSpot;
    private Piece piece;
    private int x;
    private char y;

    /**
     * Represents one spot on game board.
     *
     * @param piece
     * @param x {1,2,3,4,5,6,7,8}
     * @param y {a,b,c,d,e,f,g,h}
     */
    public Spot(Piece piece, int x, char y, TypeOfSpot typeOfSpot) {
        this.piece = piece;
        this.x = x;
        this.y = y;
        this.typeOfSpot = typeOfSpot;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(char y) {
        this.y = y;
    }

    public Piece getPiece() {
        return piece;
    }

    public int getX() {
        return x;
    }

    public char getY() {
        return y;
    }
}
